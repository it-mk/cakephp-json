<?php
namespace MK\Json;

use Cake\Core\BasePlugin;
use Cake\Core\PluginApplicationInterface;
use Cake\Database\Type;



class Plugin extends BasePlugin
{
    
    public function bootstrap(PluginApplicationInterface $app)
    {
        // Add constants, load configuration defaults.
        // By default will load `config/bootstrap.php` in the plugin.
        parent::bootstrap($app);
        
        //Type::getMap('json', 'MK\Json\Database\Type\JsonType');

        require __DIR__ . '/../config/helpers.php';
        
    }

}