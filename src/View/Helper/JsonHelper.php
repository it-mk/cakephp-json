<?php
namespace MK\Json\View\Helper;

use Cake\View\View;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\ORM\Association;
use Cake\Utility\Hash;
use Cake\Core\Configure;
use Cake\View\StringTemplate;

class JsonHelper extends Helper
{
    
    public $helpers = ['Form','Html'];
    
	/**
     * StringTemplate instance.
     *
     * @var \Cake\View\StringTemplate
     */
    protected $_templates;
    
	/**
     * Default config for this helper.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'templates' => [
            'editor' => '<div id="{{id}}" class="json-helper json-helper--fieldset {{class}}"></div>',
            'display' => '<div class="json-helper json-helper--display {{class}}">{{content}}</div>'
        ]
    ];
    
    /**
     * Constructor hook method.
     *
     * Implement this method to avoid having to overwrite the constructor and call parent.
     *
     * @param array $config The configuration settings provided to this helper.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->_templates = new StringTemplate($this->config('templates'));
    }
    
    /**
     * 
     * @param string $value
     * @param array $schema
     * @param array $options
     * @return type
     */
    public function editor($value,$schema,$options=[])
    {
        $content = '';
        $templateVars = (!empty($options['templateVars'])) ? $options['templateVars'] : [];
        if(!is_array($options)) {
            $options = [];
        }
        if(empty($options['id'])) {
            $options['id'] = 'jh-'. uniqid();
        }
        $templateVars['id'] = $options['id'];
        // JavaScript
        $jsonSchema = arrayToJson($schema);
        $js = <<<EOT
            (function (document) {
                document.addEventListener("DOMContentLoaded", function(event) {
                    var jsonEditor = new JSONEditor(document.getElementById('{$options['id']}'),
                        {$jsonSchema});
                });
            }(document));
EOT;

        $this->Html->scriptBlock($js,['block' => 'page-scripts']);
        
        // Output
        return $this->_templates->format('editor',$templateVars);
    }
    
}
